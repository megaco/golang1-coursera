package main

import (
	"os"
	"io"
	"fmt"
	"sort"
	"strconv"
)

const (
	first  = "├───"
	middle = "│"
	last   = "└───"
)

func ReadDir(dirname string, printFiles bool) ([]os.FileInfo, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	list, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return nil, err
	}

	if !printFiles {
		list_dir := list[:0]
		for _, x := range list {
			if x.IsDir() == true {
				list_dir = append(list_dir, x)
			}
		}
		list = list_dir
	}
	sort.Slice(list, func(i, j int) bool { return list[i].Name() < list[j].Name() })
	return list, nil
}

func printFile(out io.Writer, f os.FileInfo, printFiles bool, separator string) {
	if printFiles && !f.IsDir() {
		var size string
		if f.Size() == 0 {
			size = "empty"
		} else {
			size = strconv.FormatInt(f.Size(), 10) + "b"
		}
		fmt.Fprintf(out, separator+f.Name()+" ("+size+")\n")
	} else {
		fmt.Fprintf(out, separator+f.Name()+"\n")
	}
}

func createSeparators(out io.Writer, level int, m map[int]bool) {
	for i := 0; i < level; i++ {
		if m[i] == false {
			fmt.Fprintf(out, middle+"\t")
		} else {
			fmt.Fprintf(out, "\t")
		}
	}
}

func printTree(out io.Writer, path string, printFiles bool, level int, m map[int]bool) error {
	files, err := ReadDir(path, printFiles)
	if err != nil {
		fmt.Errorf(err.Error())
	}
	for i, file := range files {

		if !(i == len(files)-1) {
			m[level] = false
			createSeparators(out, level, m)
			printFile(out, file, printFiles, first)
		} else {
			m[level] = true
			createSeparators(out, level, m)
			printFile(out, file, printFiles, last)
		}
		printTree(out, path+string(os.PathSeparator)+file.Name(), printFiles, level+1, m)
	}
	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	v := make(map[int]bool)
	err := printTree(out, path, printFiles, 0, v)
	return err
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
