package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"encoding/xml"
	"os"
	"strconv"
	"encoding/json"
	"strings"
	"time"
)

type TestCase struct {
	SR SearchRequest
}

type Data struct {
	Items []Item `xml:"row"`
}

type Item struct {
	ID        string `xml:"id"`
	Age       string `xml:"age"`
	FirstName string `xml:"first_name"`
	LastName  string `xml:"last_name"`
	Gender    string `xml:"gender"`
	About     string `xml:"about"`
}

func IsValidOrder(order string) bool {
	switch order {
	case
		"Id",
		"Age",
		"Name",
		"":
		return true
	}
	return false
}

func getDataSet(path string) (*Data, error) {
	f, err := os.Open(path)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	body, _ := ioutil.ReadAll(f)
	rss := new(Data)
	err = xml.Unmarshal(body, rss)
	if err != nil {
		return nil, err
	}
	return rss, nil
}

func SearchServer(w http.ResponseWriter, r *http.Request) {
	at := r.Header.Get("AccessToken")
	if at != "GOOD_TOKEN" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	orderField := r.URL.Query().Get("order_field")
	if !IsValidOrder(orderField) {
		response := SearchErrorResponse{"ErrorBadOrderField"}
		b, _ := json.Marshal(response)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(b)
		return
	}

	query := r.URL.Query().Get("query")

	data, err := getDataSet("./dataset.xml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var users []User
	var u User
	for _, d := range data.Items {
		id, _ := strconv.Atoi(d.ID)
		age, _ := strconv.Atoi(d.Age)
		s := strings.Contains(d.About, query) || strings.Contains(d.FirstName+d.LastName, query)
		if s {
			u = User{id, d.FirstName + " " + d.LastName, age, d.About, d.Gender}
			users = append(users, u)
		}
	}
	b, _ := json.Marshal(users)
	w.Write(b)
}

func TestFindUsersBadToken(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()
	sc := SearchClient{"BAD_TOKEN", ts.URL}
	_, err := sc.FindUsers(SearchRequest{Limit: 10})
	if err == nil {
		t.Errorf("Expected %d error, got nil", http.StatusUnauthorized)
	}
}

func TestFindUsersOrderFields(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()
	sc := SearchClient{"GOOD_TOKEN", ts.URL}
	_, err := sc.FindUsers(SearchRequest{Limit: 10, OrderField: "UNKNOWN"})
	if err == nil {
		t.Error("Expected error, got nil")
	}
}

func TestFindUsersUnknownError(t *testing.T) {
	sc := SearchClient{"", ""}
	_, err := sc.FindUsers(SearchRequest{})
	if err == nil {
		t.Error("Expected error, got nil")
	}
}

func TestFindUsersTimeout(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(1 * time.Second)
	}))
	defer ts.Close()
	sc := SearchClient{"", ts.URL}
	_, err := sc.FindUsers(SearchRequest{})
	if err == nil {
		t.Error("Expected error, got nil")
	}

}

func TestFindUsersWrongJson(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, _ := json.Marshal(true)
		w.Write(b)
	}))
	defer ts.Close()
	sc := SearchClient{"", ts.URL}
	_, err := sc.FindUsers(SearchRequest{Limit: 1})
	if err == nil {
		t.Errorf("Expected error, got nil")
	}
}

func TestFindUsersLimit(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()
	sc := SearchClient{"GOOD_TOKEN", ts.URL}
	response, err := sc.FindUsers(SearchRequest{Limit: 2, Query: "Magna"})
	if err != nil {
		t.Errorf("Expected response, got %s", err)
	}
	if len(response.Users) != 2 {
		t.Errorf("Expected 2 users, got %d", len(response.Users))
	}
}

func TestFindUsersBadRequest(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
	}))
	defer ts.Close()
	sc := SearchClient{"", ts.URL}
	_, err := sc.FindUsers(SearchRequest{Limit: 1})
	if err == nil {
		t.Errorf("Expected error, got nil")
	}
}

func TestFindUsersUnknownBadRequest(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := SearchErrorResponse{"UnknownBadRequest"}
		b, _ := json.Marshal(response)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(b)
		return
	}))
	defer ts.Close()
	sc := SearchClient{"", ts.URL}
	_, err := sc.FindUsers(SearchRequest{Limit: 1})
	if err == nil {
		t.Errorf("Expected error, got nil")
	}
}

func TestFindUsersFatalServerError(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := getDataSet("./NoDataSet.xml")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}))
	defer ts.Close()
	sc := SearchClient{"", ts.URL}
	_, err := sc.FindUsers(SearchRequest{Limit: 1})
	if err == nil {
		t.Errorf("Expected error, got nil")
	}
}

func TestFindUsersLimiAndOffset(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	cases := []TestCase{
		{SearchRequest{Limit: -1, Offset: 1}},
		{SearchRequest{Limit: 30, Offset: 100}},
		{SearchRequest{Offset: -1}},
	}

	sc := SearchClient{"GOOD_TOKEN", ts.URL}

	for caseNumber, item := range cases {
		_, err := sc.FindUsers(item.SR)
		if err == nil && item.SR.Limit < 0 {
			t.Errorf("[%d] expected error, got nil", caseNumber)
		} else if err == nil && item.SR.Offset < 1 {
			t.Errorf("[%d] expected error, got nil", caseNumber)
		} else if err == nil {
			t.Logf("[%d] caseNumber has been passed ", caseNumber)
		}
	}
}
