package main

import (
	"io"
	"os"
	"strings"
	"fmt"
	"github.com/mailru/easyjson/jlexer"
	"bufio"
	"runtime/debug"
)


type User struct {
	Email    string   `json:"email"`
	Browsers []string `json:"browsers"`
	Name     string   `json:"name"`
}

func FastSearch(out io.Writer) {
	debug.SetGCPercent(-1)

	file, err := os.Open("./data/users.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()


	seenBrowsers := make(map[string]bool)
	var i = 0

	fmt.Fprintln(out, "found users:")

	reader := bufio.NewReader(file)
	for  {
		l, _, err := reader.ReadLine()
		if err != nil {
			break
		}

		user := &User{}
		err = user.UnmarshalJSON(l)
		if err != nil {
			panic(err)
		}

		isAndroid := false
		isMSIE := false

		for _, browser := range user.Browsers {

			if strings.Contains(browser, "Android") {
				isAndroid = true
				seenBrowsers[browser] = true
			}

			if strings.Contains(browser, "MSIE") {
				isMSIE = true
				seenBrowsers[browser] = true
			}
		}

		if isMSIE && isAndroid {
			email := strings.Replace(user.Email, "@", " [at] ", 1)
			fmt.Fprintln(out, fmt.Sprintf("[%d] %s <%s>", i, user.Name, email))
		}
		i++
	}

	fmt.Fprintln(out, "\nTotal unique browsers", len(seenBrowsers))
}

func easyjson3486653aDecodeCourseraGoHw3BenchFast(in *jlexer.Lexer, out *User) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "email":
			out.Email = string(in.String())
		case "browsers":
			if in.IsNull() {
				in.Skip()
				out.Browsers = nil
			} else {
				in.Delim('[')
				if out.Browsers == nil {
					if !in.IsDelim(']') {
						out.Browsers = make([]string, 0, 4)
					} else {
						out.Browsers = []string{}
					}
				} else {
					out.Browsers = (out.Browsers)[:0]
				}
				for !in.IsDelim(']') {
					var v1 string
					v1 = string(in.String())
					out.Browsers = append(out.Browsers, v1)
					in.WantComma()
				}
				in.Delim(']')
			}
		case "name":
			out.Name = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}

func (v *User) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson3486653aDecodeCourseraGoHw3BenchFast(&r, v)
	return r.Error()
}

func (v *User) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson3486653aDecodeCourseraGoHw3BenchFast(l, v)
}
