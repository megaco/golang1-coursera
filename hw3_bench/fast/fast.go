package fast

//easyjson:json
type User struct {
	Email string `json:"email"`
	Browsers []string `json:"browsers"`
	Name string `json:"name"`
}
