package main

import (
	"strconv"
	"strings"
	"sort"
	"sync"
)

func ExecutePipeline(jobs ...job) {
	wg := &sync.WaitGroup{}
	c1 := make(chan interface{})

	for _, j := range jobs {
		wg.Add(1)
		c2 := make(chan interface{})
		go func(c1, c2 chan interface{}, j job) {
			defer wg.Done()
			j(c1, c2)
			close(c2)
		}(c1, c2, j)
		c1 = c2
	}
	wg.Wait()
}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}
	cs2 := make(chan string)

	for data := range in {
		wg.Add(1)
		go func(data int) {
			defer wg.Done()
			d1 := strconv.Itoa(data)
			mu.Lock()
			md5 := DataSignerMd5(d1)
			mu.Unlock()
			go func() { cs2 <- DataSignerCrc32(d1) }()
			d3 := DataSignerCrc32(md5)
			d2 := <-cs2
			result := d2 + "~" + d3
			out <- result
		}(data.(int))
	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}

	for data := range in {
		wg.Add(1)
		go func(data string) {
			defer wg.Done()
			wg2 := &sync.WaitGroup{}
			s := make([]string, 6)
			for j := 0; j <= 5; j++ {
				wg2.Add(1)
				go func(j int) {
					defer wg2.Done()
					s[j] = DataSignerCrc32(strconv.Itoa(j) + data)
				}(j)
			}
			wg2.Wait()
			out <- strings.Join(s, "")
		}(data.(string))
	}
	wg.Wait()
}

func CombineResults(in, out chan interface{}) {
	var s []string
	for data := range in {
		s = append(s, data.(string))
	}
	sort.Strings(s)
	out <- strings.Join(s, "_")
}
